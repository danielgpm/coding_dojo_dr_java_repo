package org.dojo.test;

import static org.junit.Assert.*;

import org.dojo.RomanConverter;
import org.junit.Before;
import org.junit.Test;

public class RomanConverterTest {
  RomanConverter converter;
  
  @Before
  public void setUp() {
    converter = new RomanConverter();
  }
    
  @Test
  public void testBlankNumShouldReturnEmptyStr() {        
    assertEquals("",converter.convertToRoman(0));        
  }
  
  @Test
  public void testNumber3ShouldReturnRomanIII() {            
    assertEquals("III",converter.convertToRoman(3));    
  }
  
  @Test
  public void testNumber4ShouldReturnRomanIV() {        
    assertEquals("IV",converter.convertToRoman(4));   
  }
  
  @Test
  public void testNumber5ShouldReturnRomanV() {        
    assertEquals("V",converter.convertToRoman(5));   
  }
  
  @Test
  public void testNumber8ShouldReturnRomanVIII() {        
    assertEquals("VIII",converter.convertToRoman(8));   
  }
  
  @Test
  public void testNumber9ShouldReturnRoman() {        
    assertEquals("XCI",converter.convertToRoman(91));   
  }
  
  @Test
  public void testNumber35ShouldReturnRomanXXXV() {            
    assertEquals("XXXV",converter.convertToRoman(35));    
  }
  
  @Test
  public void testNumber40ShouldReturnRomanXL() {        
    assertEquals("XL",converter.convertToRoman(40));   
  }
  
  @Test
  public void testNumber50ShouldReturnRomanL() {        
    assertEquals("L",converter.convertToRoman(50));   
  }
  
  @Test
  public void testNumber89ShouldReturnRomanLXXXIX() {        
    assertEquals("LXXXIX",converter.convertToRoman(89));   
  }
  
  @Test
  public void testNumber90ShouldReturnRomanXC() {        
    assertEquals("XC",converter.convertToRoman(90));   
  }
  
  @Test
  public void testNumber3999ShouldReturnRomanMMMCMXCIX() {        
    assertEquals("MMMCMXCIX",converter.convertToRoman(3999));   
  }
        
}
