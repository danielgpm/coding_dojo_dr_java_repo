package org.dojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RomanConverter {

  // Unit, tens, hundreds, thousands symbols maps
  HashMap<Integer, String[]> romanMap = new HashMap<Integer, String[]>() {
    {
      put(0, new String[] { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" });
      put(1, new String[] { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" });
      put(2, new String[] { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" });
      put(3, new String[] { "", "M", "MM", "MMM" });
    }
  };

  public String convertToRoman(int number) {
    StringBuilder romanStr = new StringBuilder();

    if (number >= 0 && number <= 3999) {      
      List<Integer> digits = numToDigitList(number);      
      for (int i = digits.size() - 1, numOrder = 0; i >= 0; i--, numOrder++) {
        romanStr.insert(0, romanMap.get(numOrder)[digits.get(i)]);
      }      
    }

    return romanStr.toString();
  }

  private static List<Integer> numToDigitList(int number) {
    List<Integer> digitList = new ArrayList<Integer>();

    char[] digitChars = intToCharArray(number);
    for (char c : digitChars) {
      digitList.add(c - '0');
    }
    
    return digitList;
  }

  private static char[] intToCharArray(int number) {
    return String.valueOf(number).toCharArray();
  }
}
